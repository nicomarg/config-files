let g:tex_flavor='latex'
let g:tex_indent_items = 0
" let g:vimtex_indent_lists = []
let g:vimtex_mappings_prefix = '<localleader>'
let g:vimtex_view_method='zathura'
" let g:quickfix_enabled = 0
let g:vimtex_quickfix_open_on_warning = 0
let g:vimtex_quickfix_ignore_filters = [
    \ 'LaTeX hooks Warning',
    \ 'Underfull \\hbox',
    \ 'Overfull \\hbox',
    \ 'LaTeX Warning: .\+ float specifier changed to',
    \ 'Package siunitx Warning: Detected the "physics" package:',
    \ 'Package hyperref Warning: Token not allowed in a PDF string',
    \ '.*Package ExtSizes Warning: .*',
    \]
let g:vimtex_format_enabled = 1
UltiSnipsAddFiletypes texmath

nmap yae <plug>(vimtex-env-surround-operator)
nmap dsm <plug>(vimtex-env-delete-math)
nmap csm <plug>(vimtex-env-change-math)
nmap tsm <plug>(vimtex-env-toggle-math)
xmap am <plug>(vimtex-a$)
omap am <plug>(vimtex-a$)
xmap im <plug>(vimtex-i$)
omap im <plug>(vimtex-i$)
xmap ai <plug>(vimtex-am)
omap ai <plug>(vimtex-am)
xmap ii <plug>(vimtex-im)
omap ii <plug>(vimtex-im)
map ]$ <plug>(vimtex-]n)
map [$ <plug>(vimtex-[n)
map ]£ <plug>(vimtex-]N)
map [£ <plug>(vimtex-[N)

call deoplete#custom#var('omni', 'input_patterns', {
      \ 'tex': g:vimtex#re#deoplete
      \})

let g:vimtex_compiler_method='latexmk'
let g:vimtex_compiler_latexmk = {
    \ 'backend' : 'jobs',
    \ 'background' : 1,
    \ 'build_dir' : '',
    \ 'callback' : 1,
    \ 'continuous' : 1,
    \ 'executable' : 'latexmk',
    \ 'hooks' : [],
    \ 'options' : [
    \   '-pdf',
    \   '-bibtex',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \   '-shell-escape',
    \ ],
    \}

" forward search
function! s:TexFocusVim() abort
  " Give window manager time to recognize focus moved to Zathura;
  " tweak the 200m as needed for your hardware and window manager.
  sleep 50m

  " Refocus Vim and redraw the screen
  silent execute "!xdotool windowfocus " . expand(g:vim_window_id)
  redraw!
endfunction
augroup vimtex_event_focus
  au!
  au User VimtexEventView call s:TexFocusVim()
augroup END

" server for backward search
if empty(v:servername) && exists('*remote_startserver')
  call remote_startserver('VIM')
endif

if !exists("g:vim_window_id")
  let g:vim_window_id = system("xdotool getactivewindow")
endif

" Custom conceal
let g:vimtex_syntax_custom_cmds = [
      \ { 'name': 'R', 'mathmode': v:true, 'concealchar': 'ℝ' },
      \ { 'name': 'N', 'mathmode': v:true, 'concealchar': 'ℕ' },
      \ { 'name': 'Z', 'mathmode': v:true, 'concealchar': 'ℤ' },
      \ { 'name': 'Q', 'mathmode': v:true, 'concealchar': 'ℚ' },
      \ { 'name': 'C', 'mathmode': v:true, 'concealchar': 'ℂ' },
      \ { 'name': 'B', 'mathmode': v:true, 'concealchar': '𝔹' },
      \ { 'name': 'llbracket', 'mathmode': v:true, 'concealchar': '⟦' },
      \ { 'name': 'rrbracket', 'mathmode': v:true, 'concealchar': '⟧' }
      \ ]
let g:vimtex_syntax_custom_cmds_with_concealed_delims = [
      \ { 'name': 'Eval', 'mathmode': v:true, 'cchar_open': '⟦', 'cchar_close': '⟧' },
      \ { 'name': 'norm', 'mathmode': v:true, 'cchar_open': '∥', 'cchar_close': '∥' },
      \ { 'name': 'abs', 'mathmode': v:true, 'cchar_open': '|', 'cchar_close': '|' }
      \ ]
