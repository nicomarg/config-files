let g:coqtail_noimap = 1
let g:coqtail_match_shift = 1
nmap <buffer> <M-Up> <Plug>CoqUndo<Plug>CoqJumpToEnd
nmap <buffer> <M-Down> <Plug>CoqNext<Plug>CoqJumpToEnd
nmap <buffer> <M-Right> <Plug>CoqToLine
imap <buffer> <M-Up> <Plug>CoqUndo<Plug>CoqJumpToEnd<Right>
imap <buffer> <M-Down> <Plug>CoqNext<Plug>CoqJumpToEnd<Right>
imap <buffer> <M-Right> <Plug>CoqToLine
