" Installs vim-plug if not present
if empty(glob('~/.vim/autoload/plug.vim'))
  silent execute '!curl -fLo ~/.vim/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/bundle')
Plug 'tpope/vim-sensible'

" appearance
Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'
let g:lightline = {}
let g:lightline.colorscheme = 'jellybeans'
let g:lightline.component = { "charvaluehex": "0x%B" }
let g:lightline.component_function = { 'gitbranch': 'FugitiveHead' }
let g:lightline.component_expand = {
      \  'linter_checking': 'lightline#ale#checking',
      \  'linter_infos': 'lightline#ale#infos',
      \  'linter_warnings': 'lightline#ale#warnings',
      \  'linter_errors': 'lightline#ale#errors',
      \  'linter_ok': 'lightline#ale#ok',
      \ }
let g:lightline.component_type = {
      \     'linter_checking': 'right',
      \     'linter_infos': 'right',
      \     'linter_warnings': 'warning',
      \     'linter_errors': 'error',
      \     'linter_ok': 'right',
      \ }
let g:lightline.active = {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ],
      \   'right': [ [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ],
      \              [ 'lineinfo' ],
      \              [ 'charvaluehex', 'fileformat', 'fileencoding', 'filetype', 'percent'] ]
      \ }
set noshowmode
Plug 'flazz/vim-colorschemes'

" Efficiency bindings
Plug 'easymotion/vim-easymotion'
let mapleader = ' '
map gs <Plug>(easymotion-prefix)
map <leader> <Plug>(easymotion-prefix)
map <Plug>(easymotion-prefix)gs <Plug>(easymotion-jumptoanywhere)
map <Plug>(easymotion-prefix)s <Plug>(easymotion-f2)
map <Plug>(easymotion-prefix)S <Plug>(easymotion-F2)
map <Plug>(easymotion-prefix)/ <Plug>(easymotion-sn)
map <Plug>(easymotion-prefix)<space> <Plug>(easymotion-lineanywhere)
map <Plug>(easymotion-prefix)h <Plug>(easymotion-linebackward)
map <Plug>(easymotion-prefix)l <Plug>(easymotion-lineforward)
map <Plug>(easymotion-prefix)<Left> <Plug>(easymotion-linebackward)
map <Plug>(easymotion-prefix)<Down> <Plug>(easymotion-j)
map <Plug>(easymotion-prefix)<Up> <Plug>(easymotion-k)
map <Plug>(easymotion-prefix)<Right> <Plug>(easymotion-lineforward)
let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_upper = 1
Plug 'justinmk/vim-sneak'
let g:sneak#s_next = 1
let g:use_ic_scs = 1
map f <Plug>Sneak_f
map F <Plug>Sneak_F
map t <Plug>Sneak_t
map T <Plug>Sneak_T
Plug 'tpope/vim-unimpaired'
Plug 'qpkorr/vim-bufkill'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'wellle/targets.vim'
Plug 'tommcdo/vim-lion'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-repeat'

" Features
Plug 'preservim/nerdtree', {'on' : 'NERDTreeToggle'}
nnoremap <C-n> :NERDTreeToggle<CR>
let NERDTreeShowBookmarks=1
Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<c-f>'
let g:UltiSnipsJumpBackwardTrigger = '<c-b>'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'


" Syntax/language support
Plug 'sheerun/vim-polyglot'
Plug 'dense-analysis/ale'
let g:ale_lint_on_text_changed = 1
let g:ale_lint_delay = 2000
let g:ale_cursor_detail = 1
let g:ale_floating_preview = 1
let g:ale_echo_delay = 500
let g:ale_floating_window_border = ['│', '─', '╭', '╮', '╯', '╰', '│', '─']
function! CustomOpts() abort
  let [l:info, l:loc] = ale#util#FindItemAtCursor(bufnr(''))
  return {'title': ' ALE: ' . (l:loc.linter_name) . ' ', 'close': 'none'}
endfunction
let g:ale_floating_preview_popup_opts = 'g:CustomOpts'
let g:ale_sign_highlight_linenrs = 1
let g:ale_c_cc_options = '-Wall -Wextra -Wshadow'
let g:ale_linters_ignore = {'tex': ['chktex', 'lacheck']}
Plug 'prabirshrestha/vim-lsp'
let g:lsp_semantic_enabled = 1
Plug 'mattn/vim-lsp-settings'
let g:lsp_settings = {
      \  'ocaml-lsp': {'cmd': ['ocamllsp', '--fallback-read-dot-merlin'], },
      \  'digestif': {'disabled': 1},
      \}
function! s:on_lsp_buffer_enabled() abort
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gS <plug>(lsp-document-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> K <plug>(lsp-hover)
    nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
    nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
    " set foldmethod=expr
    "       \ foldexpr=lsp#ui#vim#folding#foldexpr()
    "       \ foldtext=lsp#ui#vim#folding#foldtext()
    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END
Plug 'rhysd/vim-lsp-ale'
Plug 'thomasfaingnaert/vim-lsp-snippets'
Plug 'thomasfaingnaert/vim-lsp-ultisnips'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'Shougo/deoplete.nvim'
let g:deoplete#enable_at_startup = 1
" highlight Pmenu ctermbg=8 guibg=#606060
" highlight PmenuSel ctermbg=1 guifg=#dddd00 guibg=#1f82cd
" highlight PmenuSbar ctermbg=0 guibg=#d6d6d6
Plug 'lighttiger2505/deoplete-vim-lsp'

" latex
Plug 'lervag/vimtex'

" coq
Plug 'whonore/Coqtail'
Plug 'joom/latex-unicoder.vim'
Plug 'chrisbra/unicode.vim'

" typst
Plug 'kaarmu/typst.vim'
call plug#end()

" Use :help 'option' to see the documentation for the given option.

set termguicolors
set background=dark
let g:gruvbox_italic=1
let g:gruvbox_number_column='bg1'
let g:gruvbox_vert_split='fg4'
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_invert_selection=0
colorscheme gruvbox

" Highlight trailing whitespaces
hi link ExtraWhitespace Error
match ExtraWhitespace /\s\+$/
augroup trailing
  au!

  autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
  autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
  autocmd InsertLeave * match ExtraWhitespace /\s\+$/
  autocmd BufWinLeave * call clearmatches()
augroup END

set conceallevel=2
set number
set autoindent
set tabstop=4
set shiftwidth=4
set expandtab
autocmd FileType ocaml,vim setlocal shiftwidth=2 tabstop=2
set tw=80

set ignorecase
set smartcase
set spell

set showcmd

if &encoding ==# 'latin1' && has('gui_running')
  set encoding=utf-8
endif

set fileformats+=mac

" Pyhton venv stuff
let g:python_host_prog = "/usr/bin/python"
let g:python3_host_prog = "/usr/bin/python"

" ## added by OPAM user-setup for vim / base ## d611dd144a5764d46fdea4c0c2e0ba07 ## you can edit, but keep this line
let s:opam_share_dir = system("opam var share")
let s:opam_share_dir = substitute(s:opam_share_dir, '[\r\n]*$', '', '')

let s:opam_configuration = {}

function! OpamConfOcpIndent()
  execute "set rtp^=" . s:opam_share_dir . "/ocp-indent/vim"
endfunction
let s:opam_configuration['ocp-indent'] = function('OpamConfOcpIndent')

function! OpamConfOcpIndex()
  execute "set rtp+=" . s:opam_share_dir . "/ocp-index/vim"
endfunction
let s:opam_configuration['ocp-index'] = function('OpamConfOcpIndex')

function! OpamConfMerlin()
  let l:dir = s:opam_share_dir . "/merlin/vim"
  execute "set rtp+=" . l:dir
endfunction
let s:opam_configuration['merlin'] = function('OpamConfMerlin')

let s:opam_packages = ["ocp-indent", "ocp-index", "merlin"]
let s:opam_available_tools = []
for tool in s:opam_packages
  " Respect package order (merlin should be after ocp-index)
  if isdirectory(s:opam_share_dir . "/" . tool)
    call add(s:opam_available_tools, tool)
    call s:opam_configuration[tool]()
  endif
endfor
" ## end of OPAM user-setup addition for vim / base ## keep this line
" ## added by OPAM user-setup for vim / ocp-indent ## 5fe0075460e1fe846a5b0d350cb248ee ## you can edit, but keep this line
if count(s:opam_available_tools,"ocp-indent") == 0
  source "/home/nicomarg/.opam/default/share/ocp-indent/vim/indent/ocaml.vim"
endif
" ## end of OPAM user-setup addition for vim / ocp-indent ## keep this line
