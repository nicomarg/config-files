#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script is a simple wrapper which prefixes each i3status line with custom
# information. It is a python reimplementation of:
# http://code.stapelberg.de/git/i3status/tree/contrib/wrapper.pl
#
# To use it, ensure your ~/.i3status.conf contains this line:
#     output_format = "i3bar"
# in the 'general' section.
# Then, in your ~/.i3/config, use:
#     status_command i3status | ~/i3status/contrib/wrapper.py
# In the 'bar' section.
#
# In its current version it will display the cpu frequency governor, but you
# are free to change it to display whatever you like, see the comment in the
# source code below.
#
# © 2012 Valentin Haenel <valentin.haenel@gmx.de>
#
# This program is free software. It comes without any warranty, to the extent
# permitted by applicable law. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License (WTFPL), Version
# 2, as published by Sam Hocevar. See http://sam.zoy.org/wtfpl/COPYING for more
# details.

import sys
import json
import subprocess
import re


def get_command_result(command):
    output = subprocess.run(['bash', '-c', command], capture_output=True).stdout
    return output.decode('utf-8')[:-1]


def format_mpc_play():
    j = []
    song = get_command_result('mpc --format="[%track%. ][%title%]|[%file%]" current')
    j.append({'full_text': song, 'name': 'mpc_song_title', 'color': '#00FFFF',
              'separator': False, 'separator_block_width': 5})

    album_artist = get_command_result('mpc --format="[([%album%][ ~ %artist%])]" current')
    j.append({'full_text': album_artist, 'name': 'mpc_album_artist',
              'color': '#FFD700'})

    rest = get_command_result('mpc --format="" status').split('\n')
    playing_info = rest[1].split()

    j.append({'full_text': playing_info[1][1:], 'name': 'mpc_queue_counter',
              'color': '#FFD700', 'separator': False, 'separator_block_width': 5})

    if playing_info[0] == '[playing]':
        play_symbol = '▶️'
        color = '#00FF00'
    else:
        play_symbol = '⏸️'
        color = '#FF0000'
    j.append({'full_text': play_symbol, 'name': 'mpc_playing', 'color': color,
              'separator': False, 'separator_block_width': 5})

    prop = rest[2].split()
    if prop[-7] == 'on':
        color = '#00FF00'
        j.append({'full_text': '🔄', 'name': 'mpc_repeat', 'color': color,
                  'separator': False, 'separator_block_width': 5})

    if prop[-5] == 'on':
        color = '#00FF00'
        j.append({'full_text': '🔀', 'name': 'mpc_random', 'color': color,
                  'separator': False, 'separator_block_width': 5})

    m = re.search(r'\d+', playing_info[3])
    progress = int(m.group(0))
    progressbar = '='*(progress//10) + '>' + '-'*(9-(progress//10))
    j.append({'full_text': progressbar[:10], 'name': 'mpc_progressbar',
              'color': '#FF4500', 'separator': False, 'separator_block_width': 5})

    l = re.split('[:/]', playing_info[2])
    length = '(' + l[2] + ':' + l[3] + ')'
    j.append({'full_text': length, 'name': 'mpc_song_len', 'color': '#FFD700',
              'separator': False, 'separator_block_width': 5})

    try:
        vol = int(rest[2][7:10])
    except ValueError:
        vol = 0
    vol_symbol = '♪:'
    if vol <= 10:
        color = '#FF0000'
    elif vol <= 50:
        color = '#FFD700'
    else:
        color = '#00FF00'

    j.append({'full_text': vol_symbol + str(vol) + '%', 'name': 'mpc_vol',
              'color': color})

    return j


def get_mpc_status():
    command = "mpc --format=\"[[%track%. ]%title%[ (%album%)][ ~ %artist%]]|[%file%]\" status"
    outputs = get_command_result(command).split('\n')
    if len(outputs) >= 3:
        return format_mpc_play()
        # """[{'full_text' : '%s' % outputs[0], 'name' : 'mpc_song'},
        #         {'full_text' : '%s' % outputs[1], 'name' : 'mpc_play'},
        #         {'full_text' : '%s' % outputs[2], 'name' : 'mpc_stats'}
        #        ]"""
    if len(outputs) == 1:
        return [{'full_text': 'No music playing', 'name': 'mpc_stats',
                 'color': '#FF0000'}]
    return [{'full_text': 'mpc problem', 'name': 'mpc_stats'}]


def print_line(message):
    """ Non-buffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()


def read_line():
    """ Interrupted respecting reader for stdin. """
    # try reading a line, removing any extra whitespace
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line
        if not line:
            sys.exit(3)
        return line
    # exit on ctrl-c
    except KeyboardInterrupt:
        sys.exit()


if __name__ == '__main__':
    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains the start of the infinite array.
    print_line(read_line())

    while True:
        line, prefix = read_line(), ''
        # ignore comma at start of lines
        if line.startswith(','):
            line, prefix = line[1:], ','

        j = json.loads(line)
        # insert information into the start of the json, but could be anywhere
        # CHANGE THIS LINE TO INSERT SOMETHING ELSE
        j = get_mpc_status() + j
        # and echo back new encoded json
        print_line(prefix+json.dumps(j))
