#!/usr/bin/env bash

playerctlstatus=$(playerctl status 2> /dev/null)
# id. title|url (album ~ artist) play/pause (position/length)
tracknumber=$(playerctl metadata xesam:trackNumber 2> /dev/null)
title=$(playerctl metadata --format "{{default(xesam:title,xesam:url)}}" 2> /dev/null)
album=$(playerctl metadata xesam:album 2> /dev/null)
artist=$(playerctl metadata artist 2> /dev/null)
position=$(playerctl position --format "{{duration(position)}}" 2> /dev/null)
length=$(playerctl metadata --format "{{duration(mpris:length)}}" 2> /dev/null)
if [[ $tracknumber ]]; then
    tracknumber="$tracknumber."
fi
if [[ $album ]]; then
    if [[ $artist ]]; then
        albumartist="($artist - $album)"
    else
        albumartist="($album)"
    fi
else
    if [[ $artist ]]; then
        albumartist="($artist)"
    else
        albumartist=""
    fi
fi
if [[ $playerctlstatus =~ "Playing" ]]; then
    playpauseicon="⏸️ "
else
    playpauseicon="▶️ "
fi
if [[ $position ]]; then
    if [[ $length ]]; then
        finallength="($position/$length)"
    else
        finallength="($position)"
    fi
else
    if [[ $length ]]; then
        finallength="($length)"
    else
        finallength=""
    fi
fi
widgettext="$playpauseicon $finallength $tracknumber $title $albumartist"
widget="%{A1:playerctl play-pause:}%{A6:playerctl next:}%{A3:playerctl previous:}"
widget="$widget%{A4:playerctl position 5+:}%{A5:playerctl position 5-:}"
widget="$widget $widgettext %{A}%{A}%{A}%{A}%{A}"
if [[ $playerctlstatus ==  "" ]]; then
    echo ""
elif [[ $playerctlstatus =~ "Stopped" ]]; then
    echo ""
else
    echo $widget
fi
