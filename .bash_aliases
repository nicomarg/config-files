alias mountd="sudo mount -t ntfs -o nls=utf8,umask=0222 /dev/sdb1 /mnt/d"
alias umountd="sudo umount -t ntfs /mnt/d"
alias mountc="sudo mount -t ntfs -o nls=utf8,umask=0222 /dev/sda3 /mnt/c"
alias umountc="sudo mount -t ntfs /mnt/c"
alias okml="rlwrap ocaml"
alias up="sudo apt update && sudo apt upgrade -y && sudo snap refresh"
alias bat="batcat"
alias dup="i3-sensible-terminal . &"

