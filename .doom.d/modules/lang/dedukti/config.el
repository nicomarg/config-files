;;; lang/dedukti/config.el -*- lexical-binding: t; -*-

;;;###package lambdapi
;; Coq-like layout, because who has large lines in short files ?
(setq lambdapi-window-layout '(h 0.6 lambdapi--temp-buffer-name (v 0.6 "*Goals*" "*lp-logs*")))

;; Doom-like keymaps, might add M-arrows, but C-c macros aren't that much more horrible to use
(map! :after lambdapi-mode
      :map lambdapi-mode-map
      :localleader
      "<right>" 'lp-prove-till-cursor
      "RET" 'lp-prove-till-cursor
      "." 'lp-prove-till-cursor
      "e" 'lp-toggle-electric-terminator
      "<up>" 'lp-proof-backward
      "p" 'lp-proof-backward
      "[" 'lp-proof-backward
      "<down>" 'lp-proof-forward
      "n" 'lp-proof-forward
      "]" 'lp-proof-forward
      "f" 'lp-jump-proof-forward
      "b" 'lp-jump-proof-backward
      "r" 'lambdapi-eglot-reconnect
      "k" 'eglot-shutdown
      "l" 'lambdapi-refresh-window-layout
      )

;;;###package eglot
(define-obsolete-function-alias
'eglot-move-to-column 'eglot-move-to-utf-16-linepos "1.12" )
