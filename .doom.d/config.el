;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!
(setq doom-font (font-spec :family "DejaVu Sans Mono" :size 15))
(setq doom-variable-pitch-font (font-spec :family "DejaVu Sans"))
(setq doom-symbol-font (font-spec :family "DejaVu Sans"))
(setq doom-serif-font (font-spec :family "DejaVu Serif"))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-gruvbox-dark-variant "hard")
(setq doom-theme 'doom-gruvbox)
;; solaire wouldn't disable itself
;; (custom-set-faces `(solaire-default-face ((t (:background ,(doom-color 'bg)))))
;;                   )
                  ;; `(hl-line ((t (:background ,(doom-color 'my-black)))))
                  ;; (solaire-hl-line-face ((t (:background ,(doom-color my-black)))))
                  ;; `(line-number-current-line ((t (:background ,(doom-color 'my-black)))))


;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(use-package! evil-snipe
  :config
  (setq! evil-snipe-repeat-scope 'whole-visible
         evil-snipe-spillover-scope 'visible))

(map! :after coq-mode
      :map coq-mode-map
      :localleader
      "<down>" #'proof-assert-next-command-interactive
      "<up>" #'proof-undo-last-successful-command
      "<right>" #'proof-goto-point
      )
(eval-after-load "proof-script"
  '(progn
     (map! :map proof-mode-map
           "M-<down>" #'proof-assert-next-command-interactive
           "M-<right>" #'proof-goto-point
           "M-<up>" #'proof-undo-last-successful-command)
     ;; proof-general colors customization
     (custom-set-faces `(proof-locked-face ((t (:background ,(doom-color 'base3)))))
                       `(proof-queue-face ((t (:background ,(doom-color 'base4)))))
                       `(coq-button-face ((t (:background ,(doom-color 'bg))))))
     )
  )

(setq! +word-wrap-disabled-modes '(so-long-mode))
(+global-word-wrap-mode +1)
;; (push '(face . nil) (get :note 'flymake-overlay-control))
;; (setq flymake-note nil)
(custom-set-faces `(flymake-note ((t (:underline ,nil)))))
